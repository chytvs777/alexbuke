'use strict';

var autoprefixer  = require('gulp-autoprefixer'),
    changed       = require('gulp-changed'),
    cssmin        = require('gulp-minify-css'),
    gulp          = require('gulp'),
    imagemin      = require('gulp-imagemin'),
    jshint        = require('gulp-jshint'),
    plumber       = require('gulp-plumber'),
    pug           = require('gulp-pug'),
    rename        = require('gulp-rename'),
    rigger        = require('gulp-rigger'),
    sass          = require('gulp-sass'),
    sourcemaps    = require('gulp-sourcemaps'),
    svgsprites    = require('gulp-svg-sprites'),
    sync          = require('browser-sync'),
    uglify        = require('gulp-uglify'),
    mergemedia    = require('gulp-merge-media-queries'),
    cleancss      = require('gulp-clean-css'),
    header        = require('gulp-header'),
    concat        = require('gulp-concat'),
    watch         = require('gulp-watch'),
    cheerio       = require('gulp-cheerio'),
    svgmin        = require('gulp-svgmin'),
    fs            = require('fs'),
    svg2png       = require('gulp-svg2png'),
    replace       = require('gulp-replace');

var options = {
    changed: changed,
    gulp: gulp,
    plumber: plumber,
    pug: pug,
    sync: sync,
    watch: watch,
    sourcemaps: sourcemaps,
    sass: sass,
    autoprefixer: autoprefixer,
    mergemedia: mergemedia,
    cleancss: cleancss,
    header: header,
    rename: rename,
    concat: concat,
    svgmin: svgmin,
    cheerio: cheerio,
    replace: replace,
    svgsprites: svgsprites,
    fs: fs,
    uglify: uglify,
    imagemin: imagemin,
    svg2png: svg2png,

    path: {
        build: {
            html: './local/templates/html/',
            js: './local/templates/html/js/',
            css: './local/templates/html/css/',
            images: './local/templates/html/images/'
        },

        src: {
            pug: './local/project/pages/**/*.pug',
            styles: [
                './local/project/assets/scss/'
            ],
            js: './local/project/assets/js/',
            svg: './local/project/svg/*.svg',
            images: './local/project/images/*.(png|jpg|jpeg|webm)',
            assets: './local/project/assets/'
        },

        watch: {
            pug: './local/project/**/*.pug',
            sass: './local/project/**/*.scss',
            js: './local/project/assets/js/**/*.js',
            svg: './local/project/svg/*.svg'
        }
    },

    serverConfig: {
        server: {
            baseDir: './local/templates/html/'
        },
        // tunnel: true,
        host: 'localhost',
        port: '2222',
        logPrefix: 'frontend'
    },

    package: require('./package.json'),
    banner: ['/*!\n' +
    ' * <%= package.name %> v<%= package.version %>\n' + // переменные берутся с package.json
    ' * <%= new Date().getFullYear() %> <%= package.author %> (<%= package.homepage %>)\n' +
    ' * Based on Bootstrap <%= package.bootstrap %>\n' +
    ' */ \n']
};

require('./gulp-tasks/pug.js')(options);
require('./gulp-tasks/sass.js')(options);
//require('./gulp-tasks/sass_vizivik.js')(options);
require('./gulp-tasks/watch.js')(options);
require('./gulp-tasks/sync.js')(options);
require('./gulp-tasks/svg.js')(options);
require('./gulp-tasks/svg2png.js')(options);
require('./gulp-tasks/imagemin.js')(options);
require('./gulp-tasks/uglify.js')(options);

gulp.task('dev', [
    'pug:build',
    'sass:build',
    'sprite:build',
    'svg2png:build',
    'imagemin:build',
    'uglify:build',
    'server',
    'watch'
]);

gulp.task('default', ['dev']);