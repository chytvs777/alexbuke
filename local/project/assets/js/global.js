var $window = $(window);
var $document = $(document);


!(function () {
    /*
     *  Grained background
     */

    'use strict';

    $window.on('load', function () {

    });

    $window.on('resize', function () {
        widthBreadcrumbs();
        // mobBgPreview();
    });

    $document.ready(function () {
        previwSlider();
        companySlider();
        widthBreadcrumbs();
        posterSlider();
        // mobBgPreview();
        //brandsSlider();
    });

    $window.on('scroll', function () {
        fixedMenu();
    });

    $document.on('click', '.menu-btn', openMobMenu);
    $document.on('click', '.mob-menu__close, .fogging', closeMobMenu);
    $document.on('click', '.short-btn', openShortMenu);    $document.on('click', '.short-menu__close, .fogging', closeShortMenu);
    $document.on('click', '.short-btn', openShortMenu);
    $document.on('click', '.btn-search', openSearchMenu);
    $document.on('click', '.search-section__close', closeSearchMenu);
    $document.on('click', '.chek-all.deactive', checkAllactive);
    $document.on('click', '.chek-all.active', checkAlldeactive);
    $document.on('click', '.mob-arrow', toggleMobDropMenu);
    $document.on('click', '.name-section--subscription .name-section__name', toggleSectionName);
    $document.on('click', '.sidebar__name', toggleSidebar);
    $document.on('click', '.js--filter', toggleFilter);
    $document.on('click', '.filter__close', closeFilter);


})();


function closeFilter() {
    $('.filter').removeClass('filter--open')
}

function toggleFilter() {
    $('.filter').toggleClass('filter--open')
}

function toggleSidebar() {
    $(this).closest(".sidebar").toggleClass("open");
}

function toggleMobDropMenu() {
    $(this).closest(".mob-menu__item").toggleClass("open-drop");
    $(this).toggleClass("active");
}

$(".lk-form .form-group__input").change(function () {
    var i = $(this).val();
    if (i.length) {
        console.log("+");
        $(this).addClass("form-group__input--focus");
    }
    else {
        console.log("-");
        $(this).removeClass("form-group__input--focus");
    }
});

$(".search-section__input").change(function () {
    var i = $(this).val();
    if (i.length) {
        $('.search-section__input-inner').addClass("search-section__input-inner--focus");
    }
    else {
        $('.search-section__input-inner').removeClass("search-section__input-inner--focus");
    }
});

$(".brand__images, .brand__name").hover(function () {
    $(this).closest('.brand').addClass("brand--hover");
}, function () {
    $('.brand').removeClass("brand--hover");
});

$(".small-item__images, .small-item__name").hover(function () {
    $(this).closest('.small-item').addClass("small-item--hover");
}, function () {
    $('.small-item').removeClass("small-item--hover");
});

$(".small-info__images, .small-info__name").hover(function () {
    $(this).closest('.small-info').addClass("small-info--hover");
}, function () {
    $('.small-info').removeClass("small-info--hover");
});

$(".big-slide__collection, .big-slide__name").hover(function () {
    $(this).closest('.big-slide').addClass("big-slide--hover");
}, function () {
    $('.big-slide').removeClass("big-slide--hover");
});

$(".big-item__collection a, .big-item__name a").hover(function () {
    $(this).closest('.big-item').addClass("big-item--hover");
}, function () {
    $('.big-item').removeClass("big-item--hover");
});


function toggleSectionName(){
    $(".name-section--subscription .name-section__inner").toggleClass("active");
}

function checkAllactive() {
    $(".form-group--sub-checkbox .checkbox input[type=checkbox]").prop('checked', true);
    $('.chek-all').addClass("active");
    $('.chek-all').removeClass("deactive");
}

function checkAlldeactive() {
    $("input[type=checkbox]").prop('checked', false);
    $('.chek-all').removeClass("active");
    $('.chek-all').addClass("deactive");
}

function mobBgPreview() {

    var width = $(window).width();
    var bg = $(".small-item__inner").attr('data-bg');
    console.log(bg);
    if (width <= 767) {

        if (bg != null && bg != "") {
            console.log("aaa");
            $('.small-item__inner').css('background-image', 'url("' + bg + '")');

        }
        else{
            console.log("--");
        }
    }
}


function openSearchMenu() {
    $('body').addClass("hidden");
    $('.search-section').addClass("search-section--open");
}

function closeSearchMenu() {
    $('body').removeClass("hidden");
    $('.search-section').removeClass("search-section--open");
}

function openMobMenu() {
    $('body').addClass("fogging");
    $('.mob-menu').addClass("mob-menu--open");
}

function closeMobMenu(e) {
    if ($(this).hasClass("mob-menu__close")) {
        $('.mob-menu').removeClass("mob-menu--open");
        $('body').removeClass("fogging");
    }

    if ($(event.target).closest(".short-menu").length) {
        return;
    }

    $('.short-menu').removeClass("short-menu--open");
    $('body').removeClass("fogging");
    event.stopPropagation();
}

function openShortMenu() {
    $('body').addClass("fogging");
    $('.short-menu').addClass("short-menu--open");
}

function closeShortMenu(e) {
    if ($(this).hasClass("short-menu__close")) {
        $('.short-menu').removeClass("short-menu--open");
        $('body').removeClass("fogging");
    }

    if ($(event.target).closest(".short-menu").length) {
        return;
    }

    $('.short-menu').removeClass("short-menu--open");
    $('body').removeClass("fogging");
    event.stopPropagation();
}


function magnificInit() {
    var galleryVideo = $('.play-video-inner');
    if (galleryVideo.length) {
        console.log('+');
        $('.play-video-inner').magnificPopup({
            type: 'iframe',
            delegate: 'a', // child items selector, by clicking on it popup will open
            zoom: {
                enabled: true,
                duration: 300 // продолжительность анимации. Не меняйте данный параметр также и в CSS
            },
            gallery: {
                enabled: true,
                navigateByImgClick: true,
                preload: [0, 1] // Will preload 0 - before current, and 1 after the current image
            }
        });
    }
}

function fixedMenu() {

    var width = $( document ).width();

    if(width >= 992){
        $(this).scrollTop();

        if ($(this).scrollTop() < 350) {
            $('.header').removeClass("header--fixed");
            $('.wrapper').removeClass("wrapper--menu-fixed");
            $('.header').removeClass("header--visible");
            console.log("--");
        }

        if ($(this).scrollTop() >= 350) {
            $('.header').addClass("header--fixed");
            $('.wrapper').addClass("wrapper--menu-fixed");
            console.log("+");
        }

        if ($(this).scrollTop() >= 370) {
            $('.header').addClass("header--visible");
            console.log("++");
        }

    }
}

function companySlider() {
    var cartSlider = $('.company-slider');

    if (cartSlider.length) {
        $('.company-slider').slick({
            infinite: true,
            dots: true,
            arrows: true,
            autoplay: true,
            autoplaySpeed: 5000,
            mobileFirst: true,
            //centerMode: true,
            //variableWidth: true,
            slidesToScroll: 1,
            slidesToShow: 1,
            draggable: true,
            swipe: true,
            touchMove: true,
            //fade: true,
            cssEase: 'linear',
            speed: 500

        });
    }
}

function brandsSlider() {
    var cartSlider = $('.brands-slider');

    if (cartSlider.length) {
        $('.brands-slider').slick({
            infinite: true,
            dots: false,
            arrows: true,
            autoplay: true,
            autoplaySpeed: 5000,
            mobileFirst: true,
            //centerMode: true,
            //variableWidth: true,
            slidesToScroll: 1,
            slidesToShow: 1,
            draggable: true,
            swipe: true,
            touchMove: true,
            //fade: true,
            cssEase: 'linear',
            speed: 500

        });
    }
}

function posterSlider() {
    var cartSlider = $('.poster-slider');

    if (cartSlider.length) {
        $('.poster-slider').removeClass("poster-slider--loading")
        $('.poster-slider').slick({
            infinite: true,
            dots: false,
            arrows: true,
            autoplay: true,
            autoplaySpeed: 5000,
            mobileFirst: true,
            //centerMode: true,
            //variableWidth: true,
            slidesToScroll: 1,
            slidesToShow: 1,
            draggable: true,
            swipe: true,
            touchMove: true,
            //fade: true,
            cssEase: 'linear',
            speed: 500

        });
    }
}

function previwSlider() {
    var cartSlider = $('.preview-slider');

    if (cartSlider.length) {
        $('.preview-slider').slick({
            infinite: true,
            dots: false,
            arrows: true,
            autoplay: true,
            autoplaySpeed: 5000,
            mobileFirst: true,
            //centerMode: true,
            //variableWidth: true,
            slidesToScroll: 1,
            slidesToShow: 1,
            draggable: true,
            swipe: true,
            touchMove: true,
            //fade: true,
            cssEase: 'linear',
            speed: 500

        });
    }
}


function widthBreadcrumbs(){
    console.log('test');
    var container = $('.breadcrumb--compress').width();
    var content  = $('.breadcrumb--compress ul').width();
    var size = container - content;
    //console.log(size);
    if(size < 0){
        $( ".breadcrumb--compress li" ).each(function() {
            var new_container = $('.breadcrumb--compress').width();
            var new_content  = $('.breadcrumb--compress ul').width();
            var new_size = new_container - new_content;
            $( this).addClass('item-press');
            if(new_size >= 0){
                console.log('все ок1');
                return false;
            }
        });

        widthBreadcrumbs();
    }
    else{
        console.log('все ок2');
    }

}